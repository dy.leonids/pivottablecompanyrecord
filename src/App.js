import React from 'react';
import ReactDOM from 'react-dom';
import PivotTableUI from 'react-pivottable/PivotTableUI';
import 'react-pivottable/pivottable.css';
import readXlsxFile from 'read-excel-file'
// see documentation for supported input formats
import XLSX from 'xlsx';
const data = [['attribute', 'attribute2'], ['value1', 'value2']];

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            file: {},
            data: [],
            cols: []
        }
        this.handleFile = this.handleFile.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }


    handleChange(e) {
        const files = e.target.files;
        if (files && files[0]) this.setState({ file: files[0] });
      };
     
      handleFile() {
        /* Boilerplate to set up FileReader */
        const reader = new FileReader();
        const rABS = !!reader.readAsBinaryString;
     
        reader.onload = (e) => {
          /* Parse data */
          const bstr = e.target.result;
          const wb = XLSX.read(bstr, { type: rABS ? 'binary' : 'array', bookVBA : true });
          /* Get first worksheet */
          const wsname = wb.SheetNames[0];
          const ws = wb.Sheets[wsname];
          /* Convert array of arrays */
          const data = XLSX.utils.sheet_to_json(ws);
          console.log(data);
          this.setState({ data : data });
          /* Update state */
        //   this.setState({ data: data, cols: make_cols(ws['!ref']) }, () => {
        //     console.log(JSON.stringify(this.state.data, null, 2));
        //   });
     
        };
     
        if (rABS) {
          reader.readAsBinaryString(this.state.file);
        } else {
          reader.readAsArrayBuffer(this.state.file);
        };
      }

    render() {
        let pivot = "";
        if(this.state.data && this.state.data.length>0){
            pivot = <PivotTableUI
                        data={data}
                        onChange={s => this.setState(s)}
                        {...this.state}
                    />
                
        }

        return(
            <div>
            <label htmlFor="file">Upload an excel to Process Triggers</label>
            <br />
            <input type="file" className="form-control" id="file" onChange={this.handleChange} />
            <br />
            <input type='submit' 
              value="Process Triggers"
              onClick={this.handleFile} />

            <br/><br/><br/><br/><br/>
            ............................................................................<br/>
            {pivot}  
            </div>
               
        
        ) 
       
    }
}

export default App;